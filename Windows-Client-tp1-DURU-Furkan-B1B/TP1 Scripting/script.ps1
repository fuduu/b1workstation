# =======================================================
#Nom de l'Auteur : DURU Furkan 
# Date : 19/10/2020
# Description : Le Script permet d'afficher un résumé de l'OS
# =======================================================

systeminfo | find "Host Name"
ipconfig | find "IPv4 Address"
systeminfo | find "OS Name"
systeminfo | find "OS Version"
systeminfo | find "System Boot Time"
systeminfo | find "Total Physical Memory"
systeminfo | find "Available Physical Memory"
get-localuser
ping 8.8.8.8
