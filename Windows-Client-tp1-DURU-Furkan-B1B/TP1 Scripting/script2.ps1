# =======================================================
#Nom de l'Auteur : DURU Furkan 
# Date : 01/11/2020
# Description : Le Script permet de verrouiller l'écran et éteindre l'ordianteur
# =======================================================

shutdown -h -t 60
Write-Output "Mise en veille dans 60 secondes"
shutdown -s -t 300
Write-Output "Arret de l'ordinateur dans 5 minutes"
