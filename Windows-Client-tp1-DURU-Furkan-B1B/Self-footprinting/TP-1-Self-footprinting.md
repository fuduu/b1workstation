# TP 1

## Host OS 

```
\Users\DURU\b1workstation> systeminfo
 DESKTOP-4SJUL4L
 Microsoft Windows 10 Famille 
 10.0.19041 N/A version 19041
  x64-based PC
  
  \Users\DURU\b1workstation> Get-CimInstance win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize
  [...]
  4294967296
  [...]
Samsung
  
  ```
  ## DEVICES
  
  ```
  \Users\DURU\b1workstation> Get-WmiObject -Class Win32_Processor -ComputerName. | Select-Object -Property [a-z]*
  
  [...]
  Name                                    : Intel(R) Core(TM) i5-4210U CPU @ 1.70GHz
  NumberOfCores                           : 2
NumberOfEnabledCore                     : 2
NumberOfLogicalProcessors               : 4
   [...]
 
 Intel(R) Core(TM) i5-4210U CPU @ 1.70GHz
  Les processeurs de la série Intel® Core ™ incluent un modificateur de marque avant les parties restantes du numéro de modèle, donc i5 est un modificateur de marque que intel utilise pour montrer son niveau de performance. Un i5 est utilisée pour ses microprocesseurs de milieu-haut de gamme de chaque génération.
 4 indique donc la génération du processeur. 
  Pour la majorité des processeurs Intel®, les trois derniers chiffres du numéro de produit (210) sont le SKU. Un SKU plus élevé au sein de marques et de générations de processeurs par ailleurs identiques aura généralement plus de fonctionnalités. Cependant  les numéros de SKU ne sont pas recommandés pour la comparaison entre différentes générations ou gammes de produits. 
  U est le suffixe de la gamme de produits. 
  Le suffixe SKU est un autre indicateur clé des capacités du processeur. Ces différences restantes sont indiquées par un suffixe de ligne de produits basé sur des lettres. La série Intel® Core ™, U indique un processeur qui a été conçu pour les ordinateurs portables à faible consommation d'énergie ou les 2-en-1.
  
  
  \Users\DURU\b1workstation> Get-PnpDevice
  [...]
   Mouse           ASUS Touchpad   
   [...]
   
   \Users\DURU\b1workstation> wmic path win32_VideoController get name
  NVIDIA GeForce 820M
  
  gwmi -Class Win32_DiskDrive -Namespace 'root\CIMV2'
  
  DeviceID   : \\.\PHYSICALDRIVE0
Model      : ST1000LM024 HN-M101MBB

 
 \Users\DURU> get-partition
 PartitionNumber  DriveLetter Offset                                                                            Size Type
---------------  ----------- ------                                                                            ---- ----
1                           1048576                                                                          50 MB IFS
2                C           53477376                                                                     585.38 GB IFS
3                           628601389056                                                                    519 MB Unknown
```



## USERS

```
\Users\DURU> Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
DURU               True
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.



U\Users\DURU>  Get-LocalUser | Select *

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
DURU               True
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.

 Get-LocalUser | Select *
 
 AccountExpires         :
Description            : Compte d’utilisateur d’administration
Enabled                : False
FullName               :
PasswordChangeableDate :
PasswordExpires        :
UserMayChangePassword  : True
PasswordRequired       : True
PasswordLastSet        :
LastLogon              :
Name                   : Administrateur
SID                    : S-1-5-21-2659959199-2058231548-771886461-500
PrincipalSource        : Local
ObjectClass            : Utilisateur


Les utilisateurs AUTORITE NT (NT AUTHORITY en anglais) sont des utilisateurs systèmes intégrés à Windows avec des privilèges plus élevés que celui du compte administrateur.
Il existe plusieurs comptes AUTORITE NT et sont utilisés par Windows pour lancer des processus systèmes ou services Windows.
Cette page donne une description de ces utilisateurs AUTORITE NT, à quoi ils servent et commencer démarrer des programmes avec l'utilisateur AUTORITE NT.
(Elle est plutôt destinée aux utilisateurs avancés).

SID : Ce sont des identifiants uniques et immuables de sécurité alphanumériques assignés par un contrôleur de domaine qui identifient chaque système, utilisateur ou objet (groupe) dans un réseau ou sur une machine. Certains SID sont identiques sur tous les systèmes.

Windows autorise ou refuse des accès et des privilèges à des ressources en se basant sur des listes de contrôle d'accès.

SID a le format suivant : S-1-5-21–7623811015-3361044348-030300820-1013

```


## Processus 
```
\Users\DURU> Get-Process

Application Frame Host: 
Ce processus est lié aux applications de la plate-forme Windows universelle, également appelées applications Store - le nouveau type d'application inclus avec Windows 10. Celles-ci sont généralement acquises à partir du Windows Store, bien que la plupart des applications Windows 10 incluent Mail, Calculator, OneNote, Movies & TV, Photos et Groove Music sont également des applications UWP

Shell Experience Host:
Shell Experience Host est une partie officielle de Windows. Il est responsable de la présentation des applications universelles dans une interface fenêtrée. Il gère également plusieurs éléments graphiques de l'interface, tels que le menu Démarrer et la transparence de la barre des tâches, ainsi que les nouveaux éléments visuels de vos flyouts de zone de notification: horloge, calendrier, etc. Il contrôle même certains éléments du comportement d'arrière-plan du bureau, comme changer l'arrière-plan lorsque vous l'avez défini sur le diaporama.

WindowsInternal.ComposableShell.Experiences.TextInput :
n fichier système principal de Windows. Il s'agit d'un fichier signé Microsoft. Le programme n'a pas de fenêtre visible.

StartMenuExperienceHost:
Il s'agit d'un exe légitime et fait partie intégrante du système d'exploitation Windows. Cela montre toutes les applications par défaut de Windows 10 dans une interface graphique. Également responsable du menu Démarrer, etc.

RunTime Broker:
RunTime Broker est un processus système Windows, qui aide à gérer les permissions des applications sur votre PC entre les applications Windows et s’assure qu’elles se comportent bien. Et ce RuntimeBroker.exe (un fichier exécutable) est placé dans le dossier System32 de votre installation Windows 10.
En général, le processus ne devrait utiliser qu’une ressource CPU très faible ou quelques mégaoctets de mémoire d’un système, mais dans certains cas, un programme Windows ou un logiciel tiers défectueux pourrait amener Runtime Broker à utiliser 100% de l’utilisation CPU et jusqu’à un gigaoctet de RAM ou même plus. Et cela aura effet de grandement ralentir ou même figer votre ordinateur Windows 10. Si vous rencontrez une telle erreur sur votre installation, ne vous inquiétez pas. Nous avons la marche à suivre afin de pouvoir désactivier de manière définitive le processus Runtime Broker.

```

## Network

```
\Windows\system32> ipconfig

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : numericable.fr

Carte réseau sans fil Connexion au réseau local* 1 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Connexion au réseau local* 2 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::4826:cf1f:69b6:a850%10
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.187
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

Carte Ethernet Connexion réseau Bluetooth :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   
  
PS C:\Windows\system32> netstat -b
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    192.168.0.25:49687     40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.0.25:49781     a104-80-20-101:https   CLOSE_WAIT
 [Video.UI.exe]
  TCP    192.168.0.25:49850     wm-in-f188:5228        ESTABLISHED
 [chrome.exe]
  TCP    192.168.0.25:50081     ec2-52-68-5-112:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.0.25:50337     93.184.220.29:http     CLOSE_WAIT
 [SearchApp.exe]
  TCP    192.168.0.25:50344     93.184.220.70:https    ESTABLISHED
 [chrome.exe]
  TCP    192.168.0.25:50370     ec2-52-54-178-155:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.0.25:50371     ec2-52-54-178-155:https  ESTABLISHED
 [chrome.exe]
 
svchost.exe : Son rôle principal consiste à charger des bibliothèques de liens dynamiques (les .dll)

chrome : Navigateur 

Video.UI.exe : Interface qui permet de filmer notre écran ou le diffuser en live
```