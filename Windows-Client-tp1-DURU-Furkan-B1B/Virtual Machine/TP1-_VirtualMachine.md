# TP 1



## Machine Virtuelle 
### Configuration Post-Install

``
ip a
``
![](https://i.imgur.com/fHHoj0I.png)

```
On voit que notre carte réseau enp0s3 n'est pas allumer puisque son adrsse ip  n'apparait pas.
```
```
Pour activer la carte réseau : 
ifup enp0s3 

```

![](https://i.imgur.com/oTTkvxT.png)


```
L'Ip adresse de enp0s3 est présent donc la carte réseau est allumé 
```
```
C:\Windows\system32>ssh root@192.168.120.50
```
![](https://i.imgur.com/xzJXYRj.png)
```` 
on est bien connecté puisque quand met la commande "ip a" on peut voir nos cartes réseaux.
````


# Partage de Fichiers

## Créer le serveur (sur votre PC)

``` 
Je suis sur windows donc c'est avec Samba: 
click droit > partage > partage avancé > partager ce dossier(il faut cocher la case)
```
![](https://i.imgur.com/SfuIXLE.png)

```
Ou bien on a aussi une commande pour activer la partage : 
New-SmbShare -Name "share" -Path "C:\Users\DURU\Desktop\share"
```


## Accéder au partage depuis la VM

```
il faut tout d'abord Installer le paquet qui permet d'utiliser les partage Samba avec la commande :
yum install -y cifs-utils
```

```
Puis on crée un dossier où on accédera au partage avec la commande : 
mkdir /opt/partage

[root@localhost /]# cd opt
[root@localhost opt]# ls
partage
[root@localhost opt]#
```
```
Une fois qu'on a crée notre dossier de partage, il faut partager le dossier et le dossier partagé doit être accessible dans la VM avec la commande :

[root@localhost /]# mount -t cifs -o username=DURU,password=12345//192.168.120.1/share /opt/partage

[root@localhost /]# cd /opt/partage

[root@localhost partage]# ls

test.txt

[root@localhost partage]# cat test.txt
Partage de Fichiers
````

![](https://i.imgur.com/Vh8qKKm.png)

![](https://i.imgur.com/MdZwNnz.png)


````
On voit que notre ficher est bien partagé, et qu'il est accesible dans la VM.
````
































  
  
  
  
  

   
   
  
  
  


   
  
  
  